# backend-calendar


## Copy project
```
git clone https://gitlab.com/Kotsur/calendar-frontend.git
```

## Create MySql database
```
 CREATE DATABASE calendar_backend;
```

## Change [application.properties](src%2Fmain%2Fresources%2Fapplication.properties)
```
spring.datasource.username=login
spring.datasource.password=password
```


## Project setup
```
mvn clean install
```
## Project start
```
mvn spring-boot:run
```
