package com.example.simplecalendarsystembackend.service;

import com.example.simplecalendarsystembackend.exeption.EventException;
import com.example.simplecalendarsystembackend.model.Event;
import com.example.simplecalendarsystembackend.payload.request.EventRequest;
import com.example.simplecalendarsystembackend.payload.response.DeleteEventResponse;
import com.example.simplecalendarsystembackend.payload.response.ErrorEventResponse;
import com.example.simplecalendarsystembackend.payload.response.EventResponse;
import com.example.simplecalendarsystembackend.repository.EventRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
class EventServiceImpTest {
    @Mock
    private EventRepository eventRepository;

    @InjectMocks
    private EventServiceImp eventService;

    @Test
    void testAddNewEvent_ValidEvent() {
        LocalDateTime dateStart = LocalDateTime.now();
        LocalDateTime dateEnd = LocalDateTime.now().plusDays(20);
        EventRequest eventRequest = new EventRequest("Test Event", "Description", dateStart, dateEnd, "Location");
        Event event = new Event(eventRequest.getTitle(), eventRequest.getDescription(), eventRequest.getStartDate(), eventRequest.getEndDate(), eventRequest.getLocation());
        when(eventRepository.save(any(Event.class))).thenReturn(event);
        ResponseEntity<EventResponse> response = eventService.addNewEvent(eventRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void testAddNewEvent_not_ValidEvent() {
        EventRequest eventRequest = new EventRequest(null, "Description", null, null, "Location");
        when(eventRepository.save(any(Event.class))).thenThrow(new EventException("Error save event"));
        try {
            eventService.addNewEvent(eventRequest);
        } catch (EventException e) {
            assertEquals("Error save event", e.getMessage());
        }
    }

    @Test
    void getAllEvent() {
        ResponseEntity<List<Event>> response = eventService.getAllEvent();
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getEvent_valid() {
        Long idEvent = 1l;
        LocalDateTime dateStart = LocalDateTime.now();
        LocalDateTime dateEnd = LocalDateTime.now().plusDays(20);
        Event event = new Event(idEvent, "Title", "descr", dateStart, dateEnd, "Location");
        when(eventRepository.findById(idEvent)).thenReturn(Optional.of(event));
        ResponseEntity<Object> response = eventService.getEvent(idEvent);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new EventResponse(idEvent, "Title", "descr", dateStart, dateEnd,"Location"), response.getBody());
    }

    @Test
    void getEvent_not_valid() {
        Long idEvent = 1l;
        when(eventRepository.findById(idEvent)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = eventService.getEvent(idEvent);
        ErrorEventResponse errorEventResponse = new ErrorEventResponse(new EventException("Event not found"));
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(errorEventResponse.getErrorMap(), response.getBody());
    }

    @Test
    void updateEvent_valid() {
        Long idEvent = 1l;
        LocalDateTime dateStart = LocalDateTime.now();
        LocalDateTime dateEnd = LocalDateTime.now().plusDays(20);
        Event event = new Event(idEvent, "Title", "descr", dateStart, dateEnd, "Location");
        EventRequest eventRequest = new EventRequest( "Title", "descr", dateStart, dateEnd,"Location");
        when(eventRepository.findById(idEvent)).thenReturn(Optional.of(event));
        when(eventRepository.save(any(Event.class))).thenReturn(event);
        ResponseEntity<Object> response = eventService.updateEvent(idEvent, eventRequest);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new EventResponse(idEvent, "Title", "descr", dateStart, dateEnd,"Location"), response.getBody());
    }

    @Test
    void updateEvent_not_valid() {
        Long idEvent = 1l;
        LocalDateTime dateStart = LocalDateTime.now();
        LocalDateTime dateEnd = LocalDateTime.now().plusDays(20);
            EventRequest eventRequest = new EventRequest( "Title", "descr", dateStart, dateEnd,"Location");
        when(eventRepository.findById(idEvent)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = eventService.updateEvent(idEvent, eventRequest);
        ErrorEventResponse errorEventResponse = new ErrorEventResponse(new EventException("Event not found"));
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(errorEventResponse.getErrorMap(), response.getBody());
    }

    @Test
    void deleteEvent_valid() {
        Long idEvent = 1l;
        LocalDateTime dateStart = LocalDateTime.now();
        LocalDateTime dateEnd = LocalDateTime.now().plusDays(20);
        Event event = new Event(idEvent, "Title", "descr", dateStart, dateEnd, "Location");
        when(eventRepository.findById(idEvent)).thenReturn(Optional.of(event));
        ResponseEntity<Object> response = eventService.deleteEvent(idEvent);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(new DeleteEventResponse(idEvent, event.getTitle(), "Event delete"), response.getBody());
        verify(eventRepository, times(1)).delete(event);
    }


    @Test
    void deleteEvent_not_valid() {
        Long idEvent = 1l;
        when(eventRepository.findById(idEvent)).thenReturn(Optional.empty());
        ResponseEntity<Object> response = eventService.deleteEvent(idEvent);
        ErrorEventResponse errorEventResponse = new ErrorEventResponse(new EventException("Event not found"));
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(errorEventResponse.getErrorMap(), response.getBody());
    }
}