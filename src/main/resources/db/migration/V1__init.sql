create table event
(
    id          bigint auto_increment
        primary key,
    description varchar(1024) null,
    end_date    datetime(6)  null,
    location    varchar(255) null,
    start_date  datetime(6)  null,
    title       varchar(255) null
);

