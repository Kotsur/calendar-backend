package com.example.simplecalendarsystembackend.repository;

import com.example.simplecalendarsystembackend.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

}
