package com.example.simplecalendarsystembackend.controller;

import com.example.simplecalendarsystembackend.model.Event;
import com.example.simplecalendarsystembackend.payload.request.EventRequest;
import com.example.simplecalendarsystembackend.service.EventService;
import com.example.simplecalendarsystembackend.validations.ResponseErrorValidation;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class EventController {
    private static final Logger logger = LoggerFactory.getLogger(EventController.class);
    private final EventService eventService;
    private final ResponseErrorValidation responseErrorValidation;

    public EventController(EventService eventService, ResponseErrorValidation responseErrorValidation) {
        this.eventService = eventService;
        this.responseErrorValidation = responseErrorValidation;
    }

    @PostMapping("/events")
    public ResponseEntity<Object> createNewEvent(@Valid @RequestBody EventRequest eventRequest, BindingResult bindingResult) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) {
            return errors;
        }
        return ResponseEntity.ok(eventService.addNewEvent(eventRequest));
    }

    @GetMapping("/events")
    public ResponseEntity<List<Event>> getEvents() {
        return eventService.getAllEvent();
    }

    @GetMapping("/events/{id}")
    public ResponseEntity<Object> getEvent(@PathVariable("id") Long idEvent) {
        return eventService.getEvent(idEvent);
    }

    @PutMapping("/events/{id}")
    public ResponseEntity<Object> updateEvents(@PathVariable("id") Long idEvent, @Valid @RequestBody EventRequest eventRequest, BindingResult bindingResult) {
        ResponseEntity<Object> errors = responseErrorValidation.mapValidationService(bindingResult);
        if (!ObjectUtils.isEmpty(errors)) {
            return errors;
        }
        return eventService.updateEvent(idEvent, eventRequest);
    }

    @DeleteMapping("/events/{id}")
    public ResponseEntity<Object> deleteEvents(@PathVariable("id") Long idEvent) {
        return eventService.deleteEvent(idEvent);
    }
}
