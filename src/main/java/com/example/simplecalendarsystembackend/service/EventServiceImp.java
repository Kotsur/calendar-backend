package com.example.simplecalendarsystembackend.service;

import com.example.simplecalendarsystembackend.exeption.EventException;
import com.example.simplecalendarsystembackend.model.Event;
import com.example.simplecalendarsystembackend.payload.request.EventRequest;
import com.example.simplecalendarsystembackend.payload.response.DeleteEventResponse;
import com.example.simplecalendarsystembackend.payload.response.ErrorEventResponse;
import com.example.simplecalendarsystembackend.payload.response.EventResponse;
import com.example.simplecalendarsystembackend.repository.EventRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImp implements EventService {
    private final EventRepository eventRepository;

    public EventServiceImp(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    @Override
    public ResponseEntity<EventResponse> addNewEvent(EventRequest e) {
        Event event = new Event(
                e.getTitle(),
                e.getDescription(),
                e.getStartDate(),
                e.getEndDate(),
                e.getLocation());
        eventRepository.save(event);
        return ResponseEntity.ok(new EventResponse(event.getId(), event.getTitle(), event.getDescription(), event.getStartDate(), event.getEndDate(), event.getLocation())
        );
    }

    @Override
    public ResponseEntity<List<Event>> getAllEvent() {
        return ResponseEntity.ok(eventRepository.findAll());
    }

    @Override
    public ResponseEntity<Object> getEvent(Long idEvent) {
        try {
            Event event = eventRepository.findById(idEvent).orElseThrow(() -> new EventException("Event not found"));
            return ResponseEntity.ok(
                    new EventResponse(
                            event.getId(),
                            event.getTitle(),
                            event.getDescription(),
                            event.getStartDate(),
                            event.getEndDate(),
                            event.getLocation())
            );
        } catch (EventException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(new ErrorEventResponse(e).getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> updateEvent(Long idEvent, EventRequest eventRequest) {
        try {
            Event event = eventRepository.findById(idEvent).orElseThrow(() -> new EventException("Event not found"));
            event.setTitle(eventRequest.getTitle());
            event.setDescription(event.getDescription());
            event.setStartDate(eventRequest.getStartDate());
            event.setEndDate(eventRequest.getEndDate());
            event.setLocation(eventRequest.getLocation());
            eventRepository.save(event);
            return ResponseEntity.ok(
                    new EventResponse(
                            event.getId(),
                            event.getTitle(),
                            event.getDescription(),
                            event.getStartDate(),
                            event.getEndDate(),
                            event.getLocation())
            );
        } catch (EventException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorEventResponse(e).getErrorMap());
        }
    }

    @Override
    public ResponseEntity<Object> deleteEvent(Long idEvent) {
        try {
            Event event = eventRepository.findById(idEvent).orElseThrow(() -> new EventException("Event not found"));
            DeleteEventResponse deleteEventResponse = new DeleteEventResponse(event.getId(), event.getTitle(), "Event delete");
            eventRepository.delete(event);
            return ResponseEntity.ok(deleteEventResponse);
        }catch (EventException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(new ErrorEventResponse(e).getErrorMap());
        }
    }
}
