package com.example.simplecalendarsystembackend.service;

import com.example.simplecalendarsystembackend.model.Event;
import com.example.simplecalendarsystembackend.payload.request.EventRequest;
import com.example.simplecalendarsystembackend.payload.response.EventResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EventService {
    ResponseEntity<EventResponse> addNewEvent(EventRequest eventRequest);

    ResponseEntity<List<Event>> getAllEvent();

    ResponseEntity<Object> getEvent(Long idEvent);

    ResponseEntity<Object> updateEvent(Long idEvent, EventRequest eventRequest);

    ResponseEntity<Object> deleteEvent(Long idEvent);
}
