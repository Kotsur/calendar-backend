package com.example.simplecalendarsystembackend.payload.request;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventRequest {
    @NotEmpty(message = "Name cannot be empty")
    private String title;
    private String description;
    @NotNull(message = "Name cannot be empty")
    private LocalDateTime startDate;
    @NotNull(message = "Name cannot be empty")
    private LocalDateTime endDate;
    private String location;
}
