package com.example.simplecalendarsystembackend.payload.response;

import com.example.simplecalendarsystembackend.exeption.EventException;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ErrorEventResponse {
    private Map<String, String> errorMap = new HashMap<>();
    private EventException eventException;

    public ErrorEventResponse(EventException eventException) {
        this.eventException = eventException;
        errorMap.put("error", eventException.getMessage());
    }
}
