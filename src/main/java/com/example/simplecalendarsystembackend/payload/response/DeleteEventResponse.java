package com.example.simplecalendarsystembackend.payload.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeleteEventResponse {
    private Long id;
    private String name;
    private String message;

}
