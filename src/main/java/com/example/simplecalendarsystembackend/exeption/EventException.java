package com.example.simplecalendarsystembackend.exeption;

public class EventException extends RuntimeException{
    public EventException(String s){
        super(s);
    }
}
